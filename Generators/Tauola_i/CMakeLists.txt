################################################################################
# Package: Tauola_i
################################################################################

# Declare the package name:
atlas_subdir( Tauola_i )

# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   Control/AthenaKernel
   GaudiKernel
   Generators/GeneratorModules
   PRIVATE
   Generators/GeneratorFortranCommon
   Generators/GeneratorObjects
   Generators/GeneratorUtils
   Generators/TruthUtils )

# External dependencies:
find_package( CLHEP )
find_package( HepMC COMPONENTS HepMC HepMCfio )
find_package( Tauola )

# Disable the usage of the --as-needed linker flags:
atlas_disable_as_needed()

# Component(s) in the package:
atlas_add_library( Tauola_iLib
   src/Tauola.cxx src/Atlas_HEPEVT.cxx src/gatlas_hepevt.F
   src/Fftau.cxx src/Fftaudet.cxx src/Ffinout.cxx
   src/gfftau.F src/gffinout.F src/gfftaudet.F
   src/TauDetails.cxx src/tauface-jetset.F
   src/UserGamprt.cxx src/usergamprt.F src/phyfix.F
   src/tauola_photos_ini.F
   PUBLIC_HEADERS Tauola_i
   INCLUDE_DIRS ${TAUOLA_INCLUDE_DIRS} ${HEPMC_INCLUDE_DIRS}
   PRIVATE_INCLUDE_DIRS ${CLHEP_INCLUDE_DIRS}
   PRIVATE_DEFINITIONS -DATLAS_CMAKE
   LINK_LIBRARIES ${TAUOLA_LIBRARIES} ${HEPMC_LIBRARIES} GaudiKernel
   GeneratorModulesLib GeneratorFortranCommonLib
   PRIVATE_LINK_LIBRARIES ${CLHEP_LIBRARIES} AthenaKernel GeneratorObjects
   TruthUtils )

atlas_add_component( Tauola_i src/components/*.cxx
   LINK_LIBRARIES GaudiKernel Tauola_iLib )

# Install files from the package:
atlas_install_joboptions( share/*.py )
